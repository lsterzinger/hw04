#Comment
# coding: utf-8

# In[52]:

"""
This program plots the Planck function of a blackbody object
Aaron Kennedy 3/4/2016
"""

print "Loading modules... ",
import matplotlib.pyplot as p #import pylab as p
import numpy as np #import numpy as np
import math
print "Done..."

#Constants for Planck
h=6.626E-34      #Planck's  Constant
k=1.381E-23      #Boltzmann's Constant
c=2.988E8        #Speed of Light

#Information to Input
#Temperature of object in Kelvin
TO=40000.
TA=9000.
TG=6000.
TK=4000.

#Define function to calculate Planck emission

def B(lam,T):
    u=h*c/(lam*k*T) #To prevent overflow
    return (2*h*c*c)/((math.exp(u)-1.)*(lam**5))




#Create array of wavelengths (in meters)
length=100000
l=np.arange(0.01,1000,0.01)*10**(-6.)
print l.size
BO=np.zeros(length-1)
BA=np.zeros(length-1)
BG=np.zeros(length-1)
BK=np.zeros(length-1)

BO_trigger=False
BK_trigger=False
BG_trigger=False
BA_trigger=False

#print blackbody.size
for t in range(1, length-1):
    BO[t]=B(l[t],TO)
    BA[t]=B(l[t],TA)
    BG[t]=B(l[t],TG)
    BK[t]=B(l[t],TK)

    #Find maximums of the functions
    if BO[t]<BO[t-1] and BO_trigger==False:
        print "The wavelength of maximum emission for O star is", l[t-1]*10**6,"microns"
        BO_trigger=True
    if BA[t]<BA[t-1] and BA_trigger==False:
        print "The wavelength of maximum emission for A star is", l[t-1]*10**6,"microns"
        BA_trigger=True
    if BG[t]<BG[t-1] and BG_trigger==False:
        print "The wavelength of maximum emission for G star is", l[t-1]*10**6,"microns"
        BG_trigger=True
    if BK[t]<BK[t-1] and BK_trigger==False:
        print "The wavelength of maximum emission for K star is", l[t-1]*10**6,"microns"
        BK_trigger=True

    
p.plot(l*10**6,BO,color="black",label="O")
p.plot(l*10**6,BA,color="blue",label="A")
p.plot(l*10**6,BG,color="green",label="G")
p.plot(l*10**6,BK,color="red",label="K")

p.title("Planck Function")
p.xlabel("Wavelenth (Microns)")
p.ylabel("Blackbody Emission $ W m^{-2} \mu m^{-1} Sr^{-1} $")
p.yscale('log')
p.xscale('log')
p.legend(loc="lower right")
p.savefig("plank.png",dpi=300)
p.show()