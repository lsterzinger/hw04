import numpy
import matplotlib.pyplot as p
import math as m

def calc_flux(time,phi,delta):
    h=((2*m.pi)*(time-12))/24
    theta=m.acos(m.sin(m.radians(phi))*m.sin(m.radians(delta))+m.cos(m.radians(phi))*m.cos(m.radians(delta)))*m.cos(h)
    flux=1368*m.cos(theta)
    return flux

hour=numpy.arange(0,25,1)
fluxmsp=numpy.zeros(25)
fluxsd=numpy.zeros(25)

for i in range(0,25,1):
    fluxmsp[i]=calc_flux(hour[i],44.9,-23.5)
    fluxsd[i]=calc_flux(hour[i],32.74,-23.5)


p.plot(hour,fluxmsp,label="Minneapolis")
p.plot(hour,fluxsd, label="San Diego")
p.legend(loc="lower right")
p.xlabel("Hour")
p.ylabel("Instantaneous solar flux")
p.title("Instantaneous solar flux Minneapolis vs San Diego")
p.savefig("solarflux.png",dpi=300)
p.show()