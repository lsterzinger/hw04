import matplotlib.pyplot as p
import numpy
import math as m

def calc_dec(day):
    d=m.asin(m.sin(m.radians(23.54))*m.sin(m.radians(360./365.)*(day-81)))
    return d
def calc_halfday(delta,phi):
    phi=m.radians(phi)
    h=m.acos(-m.tan(phi)*m.tan(delta))
    time=h*24./(2.*m.pi)
    return time
timegf=numpy.zeros(365)
timekw=numpy.zeros(365)
day=numpy.arange(1,366,1)

for t in range(0,365,1):
    timegf[t]=calc_halfday(calc_dec(1+t),43)*2
    timekw[t]=calc_halfday(calc_dec(1+t),24.55)*2

p.plot(day,timegf,label="Grand Forks")
p.plot(day,timekw,label="Key West")
p.xlabel("Day")
p.ylabel("Length of daylight (Hours)")
p.title("Grand Forks vs Key West day length")
p.legend(loc="lower right")
p.savefig("daylength.png",dpi=300)
p.show()
