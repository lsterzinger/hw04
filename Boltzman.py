import matplotlib.pyplot as p
import numpy
import math

#constants
sigma=5.67*10**-8

def boltzman(T):
	F=sigma*T**4
	return F

temperature=numpy.arange(0.,50000,1)
flux=numpy.zeros(temperature.size)

print temperature.size

for t in range (0,50000):
	flux[t]=boltzman(temperature[t])

print "The flux for an O star is %E" % flux[40000]
print "The flux for an A star is %E" % flux[9000]
print "The flux for a G star is %E" % flux[6000]
print "The flux for a K star is %E" % flux[4000]

p.plot(temperature,flux)
p.title("Flux vs Temperature")
p.xlabel("Temperature (Kelvin)")
p.ylabel("Flux")
p.yscale('log')
p.savefig("boltzman.png",dpi=300)
p.show()
